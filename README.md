# Ping pong service

## Jenkins pipeline

В каталоге [pipeline](pipeline) лежат скрипты для сборки GraalVM приложения на Quarkus

- [Jenkinsfile-back](pipeline/Jenkinsfile-back.groovy) - для сборки back микросервиса
- [Jenkinsfile-front](pipeline/Jenkinsfile-front.groovy) - для сборки front микросервиса
- [Pull-request](pipeline/Pull-request.groovy) - для проверки микросервисов во время pull request

### Атрибуты

- `CRED_ID` - Credential ID в Jenkins. Логи и пароль ТУЗ для доступа в Nexus 3
- `ci_NUMBER` - КЭ вашего проекта
- `MAVEN_SETTINGS_XML` - Settings.xml в Jenkins с настройками доступа
  к [SberOSC](https://confluence.sberbank.ru/display/IH/)

## Front

### Создать native образ

```shell
mvn clean verify -Pnative \
  -Dquarkus.container-image.build=true \
  -Dquarkus.native.container-build=true \
  -Dquarkus.container-image.image=IMAGE_NAME
```

## Back

Текущие границы времени ответа GRPC-сервиса

```shell
curl http://localhost:8080/bound
```

Установка границ времени ответа

```shell
curl -d '{ "lower": 50, "upper": 200 }' http://localhost:8080/bound
```

### GRPC

Описание

```shell
grpcurl localhost:9000 list

grpcurl -plaintext localhost:9000 describe ru.sbrf.gwec.PingPong

grpcurl -plaintext localhost:9000 describe ru.sbrf.gwec.PingPong

grpcurl -plaintext localhost:9000 describe ru.sbrf.gwec.Msg
```

Запрос

```shell
grpcurl -plaintext -d '{"body": "PING"}' localhost:9000 ru.sbrf.gwec.PingPong/ping
```

## Helm-chart

### Install

```shell
helm install ping-pong helm/ping-pong
```

### Upgrade to JIT version

```shell
helm upgrade ping-pong helm/ping-pong --values helm/ping-pong/values/demo-jit.yaml
```

### Upgrade to GraalVM version

```shell
helm upgrade ping-pong helm/ping-pong --values helm/ping-pong/values/demo-native.yaml
```

### uninstall

```shell
helm uninstall ping-pong
```