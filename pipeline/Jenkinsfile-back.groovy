@Library('DPMPipelineUtils@1.4') RN
@Library('ru.sbrf.devsecops@master') DevSecOps

pipeline {

    agent {
        label "clearAgent"
    }

    tools {
        jdk 'openjdk-11'
        maven 'Maven 3.6.3'
    }

    options {
        buildDiscarder(logRotator(
                numToKeepStr: '5',
                daysToKeepStr: '5',
                artifactDaysToKeepStr: '5',
                artifactNumToKeepStr: '5'
        ))
        disableConcurrentBuilds()
        disableResume()
    }

    environment {
        CRED = credentials('CRED_ID')
        DOCKER_SERVER = "registry-ci.delta.sbrf.ru"
        DOCKER_PUSH = "docker-dev.registry-ci.delta.sbrf.ru"
        NEXUS_COMPILING_IMAGE = "${DOCKER_SERVER}/redhat/quarkus/mandrel-22-rhel8:22.3"
        PROJECT_VERSION = "D-00.001.01-${BUILD_ID}"
        IMAGE_NAME = "${DOCKER_PUSH}/ci_NUMBER/ci_NUMBER/back:${PROJECT_VERSION}"
        DISTRIB_VERSION = "${PROJECT_VERSION}_openshift"
        DISTRIB_PATH = "./devops/charts"
    }

    stages {
        stage('Get base image') {
            steps {
                sh "docker logout"
                sh "docker login --username ${CRED_USR} --password ${CRED_PSW} ${DOCKER_SERVER}"
            }
        }

        stage('Build native executable') {
            steps {
                configFileProvider([configFile(fileId: 'settings-config', variable: 'MAVEN_SETTINGS_XML')]) {
                    sh "mvn -s ${MAVEN_SETTINGS_XML} " +
                            "-Drevision=${PROJECT_VERSION} " +
                            "clean package -U " +
                            "-Pnative " +
                            "-Dquarkus.native.container-build=true " +
                            "-Dquarkus.native.builder-image=${NEXUS_COMPILING_IMAGE} "
                }
            }
        }

        stage('Build Native Image') {
            steps {
                sh "docker image build --file src/main/docker/Dockerfile.native --tag ${IMAGE_NAME} ."
            }
        }

        stage('Push image') {
            steps {
                withDockerRegistry([credentialsId: 'CRED_ID', url: "https://${DOCKER_PUSH}" ]){
                    sh "docker image push ${IMAGE_NAME}"
                    script {
                        String repoDigest = sh(script: "docker inspect --format '{{index .RepoDigests 0}}' ${IMAGE_NAME}",
                                returnStdout: true)
                        String digest = repoDigest.substring(repoDigest.lastIndexOf("@") + 1).trim()
                        writeFile file: "${DISTRIB_PATH}/image.digest", text: digest
                        writeFile file: "${DISTRIB_PATH}/image.repodigest", text: repoDigest
                        writeFile file: "${DISTRIB_PATH}/git-commit.txt", text: GIT_COMMIT
                    }
                }
            }
        }

        stage('Distrib') {
            steps {
                zip zipFile: "distrib-${DISTRIB_VERSION}.zip", dir: DISTRIB_PATH,
                        glob: 'image.*, git-commit.txt'
                configFileProvider([configFile(fileId: 'settings-config', variable: 'MAVEN_SETTINGS_XML')]) {
                    sh "mvn -s ${MAVEN_SETTINGS_XML} deploy:deploy-file \
-DgroupId=CI_NUMBER \
-DartifactId=CI_NUMBER \
-Dversion=${DISTRIB_VERSION} \
-Dclassifier=distrib \
-Dpackaging=zip \
-Dfile=distrib-${DISTRIB_VERSION}.zip \
-DgeneratePom=true \
-DrepositoryId=mavenRepo \
-Durl=https://nexus-ci.delta.sbrf.ru/repository/maven-distr-dev"
                }
            }
        }

    }

    post {
        always {
            script {
                try {
                    sh(script: "docker image rm ${IMAGE_NAME}")
                } catch (Exception e) {
                    print(e)
                }
            }
            sh "docker logout"
            cleanWs()
        }
    }
}