pipeline {
    agent {
        label 'clearAgent'
    }

    tools {
        jdk 'openjdk-11'
        maven 'Maven 3.6.3'
    }

    options {
        buildDiscarder(logRotator(
                numToKeepStr: '5',
                daysToKeepStr: '5',
                artifactDaysToKeepStr: '5',
                artifactNumToKeepStr: '5'
        ))
        disableConcurrentBuilds()
        disableResume()
    }

    stages {
        stage('Verify') {
            steps {
                script {
                    notifyBitbucket()
                }

                withSonarQubeEnv(credentialsId: 'token_sonarqube', installationName: 'SonarQube') {
                    configFileProvider([configFile(fileId: 'settings-config', variable: 'MAVEN_SETTINGS_XML')]) {
                        sh "mvn -s ${MAVEN_SETTINGS_XML} clean verify -U"
                        sh "mvn -s ${MAVEN_SETTINGS_XML} " +
                                "-Dsonar.pullrequest.branch=${PULL_REQUEST_FROM_BRANCH} " +
                                "-Dsonar.pullrequest.key=${PULL_REQUEST_ID} " +
                                "-Dsonar.scm.revision=${GIT_COMMIT} " +
                                "sonar:sonar"
                    }
                }
            }
        }
    }

    post {
        success {
            script {
                currentBuild.result = 'SUCCESS'
                notifyBitbucket()
            }
        }
        failure {
            script {
                currentBuild.result = 'FAILED'
                notifyBitbucket()
            }
        }
    }
}

def notifyBitbucket() {
    step([$class                       : 'StashNotifier',
          commitSha1                   : '',
          credentialsId                : 'CRED_ID',
          disableInprogressNotification: false,
          considerUnstableAsSuccess    : false,
          ignoreUnverifiedSSLPeer      : false,
          includeBuildNumberInKey      : false,
          prependParentProjectKey      : false,
          projectKey                   : '',
          stashServerBaseUrl           : 'https://STASH_URL'])
}